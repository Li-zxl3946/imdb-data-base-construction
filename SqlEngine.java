package hw1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import javax.print.DocFlavor.URL;

enum sqlTableType
{
	UPDATE_MOVIE,
	UPDATE_NAME,
	UPDATE_PERSON,
	UPDATE_DIRECTOR,
	UPDATE_PRODUCER,
	UPDATE_ACTOR,
	UPDATE_WRITER,
	UPDATE_MOVIE_GENRES,
	UPDATE_GENRES
}

enum dataType
{
	SET_INT, 
	SET_STRING,
	SET_DOUBLE,
	SET_YEAR
}

class sqlData
{
	public dataType type;
	public String attribute;
	public Object data;
	
	public sqlData(dataType type, String attribute, Object data)
	{
		this.type = type;
		this.attribute = attribute;
		this.data = data;
	}
}

class movieData
{
	public String name;
	public String genres;
	public double rating;
	public int votes;
}

class nameData
{
	public int id;
	public String name;
	public String birthYear;
	public String deathYear;
	
	public nameData(int id, String name, String birthYear, String deathYear)
	{
		this.id = id;
		this.name = name;
		this.birthYear = birthYear;
		this.deathYear = deathYear; 
	}
}

class personData
{
	public int movieNameId;
	public int personNameId;
	
	public personData(int movieNameId, int personNameId)
	{
		this.movieNameId = movieNameId;
		this.personNameId = personNameId;
	}	
}

class movieGenresData
{
	public int movieNameId;
	public int genresNameId;
	
	public movieGenresData(int movieNameId, int genresNameId)
	{
		this.movieNameId = movieNameId;
		this.genresNameId = genresNameId;
	}	
}

class LoadDataEngine implements Runnable
{
	private String jdbc_driver = "";
	private String db_url = "jdbc:mysql://localhost:3306/hw1?autoReconnect=true&useSSL=true";
	private String userName = "user1";
	private String password = "password1";
	private Connection db_connection = null;
	private String threadName;
	private Map<Integer, movieData> movieDataList;
	private List<nameData> nameDataList;
	private List<personData> directorDataList;
	private List<personData> producerDataList;
	private List<personData> actorDataList;
	private List<personData> writerDataList;
	private List<movieGenresData> movieGenresDataList;
	private List<String> genresList;
	private int numOfRecordToUpdate = 5000;
	private sqlTableType tableType;
	
	@Override
	public void run() 
	{
        try
        {
        	// TODO Auto-generated method stub
        	connectDatabase();
        	
        	switch(tableType)
        	{
        	case UPDATE_MOVIE:
        		updateMovieDataToDataBase();
        		break;
        		
        	case UPDATE_NAME:
        		updateNameDataToDataBase();
        		break;
        		
        	case UPDATE_PERSON:
        		updatePersonDataToDataBase();
        		break;
        		
        	case UPDATE_DIRECTOR:
        		this.updatePersonDataToDataBase("directorList", this.directorDataList);
        		this.directorDataList.clear();
        		break;

        	case UPDATE_PRODUCER:
        		this.updatePersonDataToDataBase("producerList", this.producerDataList);
        		this.producerDataList.clear();
        		break;
        		
        	case UPDATE_ACTOR:
        		this.updatePersonDataToDataBase("actorList", this.actorDataList);
        		this.actorDataList.clear();
        		break;
        		
        	case UPDATE_WRITER:
        		this.updatePersonDataToDataBase("writerList", this.writerDataList);
        		this.writerDataList.clear();
        		break;
        		
        	case UPDATE_GENRES:
        		this.updateGenresList();
        		this.genresList.clear();
        		break;
        		
        	case UPDATE_MOVIE_GENRES:
        		this.updateMovieGenresDataList();
        		this.movieGenresDataList.clear();
        		break;
        	}
        	
        	closeDatabase();

        }
        catch (Exception e)
        {
            // Throwing an exception
            System.out.println ("Exception is caught");
        }
	}
	
	public void setUpdateDataType(sqlTableType tableType)
	{
		this.tableType = tableType;
	}
	
	public void connectDatabase()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");

		    // Open a connection
		    // System.out.print("Connecting to database...");
		    db_connection = DriverManager.getConnection(db_url, userName, password);
		    if (!db_connection.isClosed() || db_connection != null)
		    {
		    	// System.out.println(" Connected");
		    }
		    
			// Do not auto commit
			db_connection.setAutoCommit(false);
			
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(SQLException e)
		{
			// System.out.println(" Fail");
			
			// Catch Exception Close DataBase	
			try
			{
				if (db_connection != null)
				{
					db_connection.close();
				}
			}
			catch (SQLException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			};
		}
	}
	
	public void closeDatabase()
	{
	    try 
	    {
			if (!db_connection.isClosed() || db_connection != null)
			{
				db_connection.close();
			}
		} 
	    catch (SQLException e)
	    {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void commitSQL()
	{
		try
		{
			// Commit Statement
			db_connection.commit();
		}
		catch(SQLException e)
		{
			try 
			{
				if (db_connection != null)
				{
					db_connection.rollback();
				}
			}
			catch (SQLException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	
	public boolean isIdExist(String tableName, int id)
	{
		boolean rtnVal = false;
		
		String SQLquery = "select * from " + tableName + " where id=" + id + ";";
		
		Statement sqlStmt = null;
		
		try
		{
			// Prepare statement 
			sqlStmt = db_connection.createStatement();
			ResultSet rs = sqlStmt.executeQuery(SQLquery);
			rtnVal = rs.next();
						
		}
		catch(SQLException e)
		{
			System.out.println("SQL select error:" + SQLquery);
		}
		finally
		{
			try 
			{
				if (sqlStmt != null)
				{
					sqlStmt.close();
				}
			} 
			catch (SQLException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		return rtnVal;
	}
	
	public void executeSQL(String SQLquery)
	{
		PreparedStatement sqlStmt = null;
		
		try
		{
			// Prepare statement 
			sqlStmt = db_connection.prepareStatement(SQLquery);
			sqlStmt.executeUpdate();
			
			// Commit Statement
			db_connection.commit();
		}
		catch(SQLException e)
		{
			System.out.println("SQL udpate error:" + SQLquery);
			
			try 
			{
				if (db_connection != null)
				{
					db_connection.rollback();
				}
			}
			catch (SQLException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		finally
		{
			try 
			{
				if (sqlStmt != null)
				{
					sqlStmt.close();
				}
			} 
			catch (SQLException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public void executeSQL(String SQLquery, List<sqlData>tableRecord)
	{
		PreparedStatement sqlStmt = null;
		
		try
		{
			// Prepare statement 
			sqlStmt = db_connection.prepareStatement(SQLquery);
			for (int i = 0; i < tableRecord.size(); i++)
			{
				int curIndex = i + 1;
				
				if (tableRecord.get(i).data != null)
				{
					switch(tableRecord.get(i).type)
					{
					case SET_INT:
						sqlStmt.setInt(curIndex, (int)tableRecord.get(i).data);
						break;
					case SET_STRING:
						sqlStmt.setString(curIndex, (String)tableRecord.get(i).data);
						break;
					case SET_DOUBLE:
						sqlStmt.setDouble(curIndex, (double)tableRecord.get(i).data);
						break;
					case SET_YEAR:
						sqlStmt.setString(curIndex, (String)tableRecord.get(i).data);
						break;
					}
				}
				else
				{
					switch(tableRecord.get(i).type)
					{
					case SET_INT:
						sqlStmt.setNull(curIndex, java.sql.Types.INTEGER);
						break;
					case SET_STRING:
						sqlStmt.setNull(curIndex, java.sql.Types.VARCHAR);
						break;
					case SET_DOUBLE:
						sqlStmt.setNull(curIndex, java.sql.Types.DOUBLE);
						break;
					case SET_YEAR:
						sqlStmt.setNull(curIndex, java.sql.Types.DATE);
						break;
					}
				}
			}
			sqlStmt.executeUpdate();
		}
		catch(SQLException e)
		{
			System.out.println(e.getMessage());
			System.out.println(this.threadName + ":SQL udpate error =>" + SQLquery);
			for (int i =0; i<tableRecord.size(); i++)
			{
				System.out.print(tableRecord.get(i).attribute + ":");
				System.out.println(tableRecord.get(i).data);
			}
		}
		finally
		{
			try 
			{
				if (sqlStmt != null)
				{
					sqlStmt.close();
				}
			} 
			catch (SQLException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public void insertToTable(String tableName, List<sqlData>tableRecord)
	{			
		// Write Up a SQL String
		String sql = "INSERT INTO " + tableName;
		String sql_att = "(";
		String sql_data = "(";
		
		for (int i=0; i < tableRecord.size(); i++)
		{
			sql_att += tableRecord.get(i).attribute;
			sql_data += "?";
			
			if  (i == tableRecord.size()-1)
			{
				sql_att += ")";
				sql_data += ")";
			}
			else
			{
				sql_att += ", ";
				sql_data += ", ";
			}
		}
		
		sql = sql + sql_att + " VALUES" + sql_data;
			
		this.executeSQL(sql, tableRecord);
	}
	
	public void setMovieDataList(Map<Integer, movieData> movieDataList)
	{
		this.movieDataList = movieDataList;
	}
	
	public void updateMovieDataToDataBase()
	{
		int count =0;
				
		// Loop through the Movie Data list
		Iterator it = this.movieDataList.entrySet().iterator();
		while (it.hasNext())
		{
			count++;
			Map.Entry<Integer, movieData> pair = (Map.Entry<Integer, movieData>) it.next();
			
			List<sqlData>tableMap = new ArrayList<sqlData>();
			
			tableMap.add(new sqlData(dataType.SET_INT, "ID", pair.getKey()));
	
			if (pair.getValue().rating == -1.0)
			{
				tableMap.add(new sqlData(dataType.SET_DOUBLE, "Rating", null));
			}
			else
			{
				tableMap.add(new sqlData(dataType.SET_DOUBLE, "Rating", pair.getValue().rating));
			}

			if (pair.getValue().votes == -1)
			{
				tableMap.add(new sqlData(dataType.SET_INT, "Vote", null));
			}
			else
			{
				tableMap.add(new sqlData(dataType.SET_INT, "Vote", pair.getValue().votes));
			}			

			this.insertToTable("movieList", tableMap);
			it.remove();
			
			if (count % this.numOfRecordToUpdate == 0)
			{
				commitSQL();
			}	
			
		}
		
		commitSQL();
		
		this.movieDataList.clear();
	}
	
	public void setNameDataList(List<nameData> nameDataList)
	{
		this.nameDataList = nameDataList;
	}

	public void updateNameDataToDataBase()
	{
		int count = 0;
		
		for (int i = 0; i < this.nameDataList.size(); i++)
		{
			count++;
			
			List<sqlData>tableMap = new ArrayList<sqlData>();
			
			tableMap.add(new sqlData(dataType.SET_INT, "id", this.nameDataList.get(i).id));
			tableMap.add(new sqlData(dataType.SET_STRING, "personName", this.nameDataList.get(i).name));
			
			if (this.nameDataList.get(i).birthYear.equals("\\N"))
			{
				tableMap.add(new sqlData(dataType.SET_STRING, "birthYear", null));
			}
			else
			{
				tableMap.add(new sqlData(dataType.SET_STRING, "birthYear", this.nameDataList.get(i).birthYear));
			}
			
			if (this.nameDataList.get(i).deathYear.equals("\\N"))
			{
				tableMap.add(new sqlData(dataType.SET_STRING, "deathYear", null));
			}
			else
			{
				tableMap.add(new sqlData(dataType.SET_STRING, "deathYear", this.nameDataList.get(i).deathYear));
			}
			
			this.insertToTable("nameList", tableMap);
			
			if (count % this.numOfRecordToUpdate == 0)
			{
				commitSQL();
			}

		}
		
		commitSQL();
		
		this.nameDataList.clear();
	}
	
	public void setDirectorDataList(List<personData> directorDataList)
	{
		this.directorDataList = directorDataList;
	}
	
	public void setProducerDataList(List<personData> producerDataList)
	{
		this.producerDataList = producerDataList;
	}
	
	public void setActorDataList(List<personData> actorDataList)
	{
		this.actorDataList = actorDataList;
	}
	
	public void setWriterDataList(List<personData> writerDataList)
	{
		this.writerDataList = writerDataList;
	}
		
	public void updatePersonDataToDataBase()
	{
		this.updatePersonDataToDataBase("directorList", this.directorDataList);
		this.directorDataList.clear();
		this.updatePersonDataToDataBase("writerList", this.writerDataList);
		this.writerDataList.clear();
		this.updatePersonDataToDataBase("actorList", this.actorDataList);
		this.actorDataList.clear();
		this.updatePersonDataToDataBase("producerList", this.producerDataList);
		this.producerDataList.clear();
	}
	
	public void updatePersonDataToDataBase(String tableName, List<personData> personDataList)
	{
		int count = 0;
		int missingIDCount = 0;
		
		for (int i = 0; i < personDataList.size(); i++)
		{
			if (	!isIdExist("nameList", personDataList.get(i).personNameId) ||
					!isIdExist("movieList", personDataList.get(i).movieNameId)	)
			{
				missingIDCount++;
				continue;
			}
			
			count++;
			List<sqlData>tableMap = new ArrayList<sqlData>();
			int threadId = Integer.parseInt(this.threadName.substring(7));
			tableMap.add(new sqlData(dataType.SET_INT, "personNameId", personDataList.get(i).personNameId));
			tableMap.add(new sqlData(dataType.SET_INT, "movieNameId", personDataList.get(i).movieNameId));
			
			this.insertToTable(tableName, tableMap);
						
			if (count % this.numOfRecordToUpdate == 0)
			{
				commitSQL();
			}
		}
		
		commitSQL();
	}

	public void setGenresList(List<String> genresList)
	{
		this.genresList = genresList;
	}
	
	public void updateGenresList()
	{
		int count = 0;
		for(int i=0; i<this.genresList.size(); i++)
		{
			List<sqlData>tableMap = new ArrayList<sqlData>();
			tableMap.add(new sqlData(dataType.SET_INT, "ID", (i+1)));
			tableMap.add(new sqlData(dataType.SET_STRING, "genres", genresList.get(i)));
			
			this.insertToTable("genresList", tableMap);
			
			if (count % this.numOfRecordToUpdate == 0)
			{
				commitSQL();
			}
		}
		
		commitSQL();
	}
	
	public void setMovieGenresDataList(List<movieGenresData> movieGenresDataList)
	{
		this.movieGenresDataList = movieGenresDataList;
	}
	
	public void updateMovieGenresDataList()
	{
		int count = 0;
		for(int i=0; i<this.movieGenresDataList.size(); i++)
		{
			count++;
			
			List<sqlData>tableMap = new ArrayList<sqlData>();
			tableMap.add(new sqlData(dataType.SET_INT, "movieNameId", movieGenresDataList.get(i).movieNameId));
			tableMap.add(new sqlData(dataType.SET_INT, "genresNameId", movieGenresDataList.get(i).genresNameId));
			
			this.insertToTable("movieGenresList", tableMap);
			
			if (count % this.numOfRecordToUpdate == 0)
			{
				commitSQL();
			}

		}
		
		commitSQL();
	}
	
	public LoadDataEngine(String threadName)
	{
		this.threadName = threadName;
	}	
}

public class SqlEngine {
	
	private String jdbc_driver = "";
	private String db_url = "jdbc:mysql://localhost:3306/hw1?autoReconnect=true&useSSL=true";
	private String userName = "user1";
	private String password = "password1";
	private Connection db_connection = null;
	private List<Map<Integer, movieData>> movieDataList;
	private List<ArrayList<nameData>> nameDataList;
	private List<ArrayList<personData>> actorDataList;
	private List<ArrayList<personData>> directorDataList;
	private List<ArrayList<personData>> writerDataList;
	private List<ArrayList<personData>> producerDataList;
	private List<LoadDataEngine> dataEngineList;
	private List<ArrayList<movieGenresData>> movieGenresDataList;
	private List<String> genresList;
	private int numOfThread;
	
	public SqlEngine(int numOfThread)
	{
		this.numOfThread = numOfThread;
		this.movieDataList = new ArrayList<Map<Integer, movieData>>();
		this.nameDataList = new ArrayList<ArrayList<nameData>>();
		this.actorDataList = new ArrayList<ArrayList<personData>>();
		this.directorDataList = new ArrayList<ArrayList<personData>>();
		this.writerDataList =  new ArrayList<ArrayList<personData>>();
		this.producerDataList = new ArrayList<ArrayList<personData>>();
		this.dataEngineList = new ArrayList<LoadDataEngine>();
		this.movieGenresDataList = new ArrayList<ArrayList<movieGenresData>>();
		this.genresList = new ArrayList<String>();
		
		for(int i =0; i < this.numOfThread; i++)
		{
			try
			{
				LoadDataEngine tmpDataEngine = new LoadDataEngine("Thread#" + Integer.toString(i));
				dataEngineList.add(tmpDataEngine);
			} 
			catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void connectDatabase()
	{
		try
		{
			Class.forName("com.mysql.jdbc.Driver");

		    // Open a connection
		    System.out.print("Connecting to database...");
		    db_connection = DriverManager.getConnection(db_url, userName, password);
		    if (!db_connection.isClosed() || db_connection != null)
		    {
		    	System.out.println(" Connected");
		    }
		    
			// Do not auto commit
			db_connection.setAutoCommit(false);
			
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		catch(SQLException e)
		{
			System.out.println(" Fail");
			// Catch Exception Close DataBase	
			try
			{
				if (db_connection != null)
				{
					db_connection.close();
				}
			}
			catch (SQLException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			};
		}
	}
	
	public void insertToTable(String tableName, Map<String, Object>tableRecord)
	{			
		// Write Up a SQL String
		String sql = "INSERT INTO " + tableName;
		String sql_att = "(";
		String sql_data = "(";
		
		Iterator<Map.Entry<String, Object>> entries = tableRecord.entrySet().iterator();
		while(entries.hasNext())
		{
			Map.Entry<String, Object> entry = entries.next();
			sql_att += entry.getKey();
			
			if (entry.getValue() instanceof String)
			{
				sql_data += "\"" + entry.getValue() + "\"";
			}
			else
			{
				sql_data += String.valueOf(entry.getValue());
			}
			
			if (entries.hasNext())
			{
				sql_att += ", ";
				sql_data += ", ";
			}
			else
			{
				sql_att += ")";
				sql_data += ")";
			}
		}
		
		sql = sql + sql_att + " VALUES" + sql_data;
				
		this.executeSQL(sql);
	}
	
	private String String(Object value, String string) {
		// TODO Auto-generated method stub
		return null;
	}

	public void getRecordFromTable()
	{
		try 
		{
			Statement sqlStmt = db_connection.createStatement();
			String sql = "SELECT * FROM Movie";
		    ResultSet rs = sqlStmt.executeQuery(sql);
		    
		    while(rs.next())
		    {
		    	int rating = rs.getInt("Rating");
		    	
		    	System.out.println("Rating:" + rating);
		    }
		}
		catch (Exception e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void executeSQL(String SQLquery)
	{
		PreparedStatement sqlStmt = null;
		
		try
		{
			// Prepare statement 
			sqlStmt = db_connection.prepareStatement(SQLquery);
			sqlStmt.executeUpdate();
			
			// Commit Statement
			db_connection.commit();
		}
		catch(SQLException e)
		{			
			try 
			{
				if (db_connection != null)
				{
					db_connection.rollback();
				}
			}
			catch (SQLException e1) 
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			System.out.println("SQL udpate error:" + SQLquery);
			System.out.println(e.getMessage());
			System.out.println("SQL database roll back state");

		}
		finally
		{
			try 
			{
				if (sqlStmt != null)
				{
					sqlStmt.close();
				}
			} 
			catch (SQLException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public void closeDatabase()
	{
	    try 
	    {
			if (!db_connection.isClosed() || db_connection != null)
			{
				db_connection.close();
			}
		} 
	    catch (SQLException e)
	    {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void clearTable(String tableName)
	{
		executeSQL("delete from " + tableName);
	}
	
	public void loadMovieData()
	{
		try 
		{
			System.out.print("Load title_basics_tsv_gz/data.tsv file...");
			Map<Integer, movieData> tmpList = new<Integer, movieData>HashMap();
			
			// Read Movie Data
			File file = new File("title_basics_tsv_gz/data.tsv");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
			String st;
			
			int lineCount = 0;
			
			// Skip the first line
			st = br.readLine();
			
			// Read the content
			while ((st = br.readLine()) != null)
			{
				lineCount++;
			    String[] str_array = st.split("\t");
			    			   
			    movieData tmpData = new movieData();
			    byte[] movieNameBytes = str_array[2].getBytes("UTF8");
			    tmpData.name = new String(movieNameBytes, "UTF8");
			    tmpData.genres = str_array[8];
			    tmpData.rating = -1.0;
			    tmpData.votes = -1;
			    
			    tmpList.put(Integer.parseInt(str_array[0].substring(2)), tmpData);
			}
			
			System.out.println("Done");
			
			System.out.print("Load title_rating_tsv_gz/data.tsv file...");
			// read vote and rating
			file = new File("title_rating_tsv_gz/data.tsv");
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
			
			// Skip the first line
			st = br.readLine();
			
			// Read the content
			while ((st = br.readLine()) != null)
			{
				String[] str_array = st.split("\t");
				tmpList.get(Integer.parseInt(str_array[0].substring(2))).rating = Double.parseDouble(str_array[1]);
				tmpList.get(Integer.parseInt(str_array[0].substring(2))).votes = Integer.parseInt(str_array[2]);
			}
			
			int maxNumOfRecord = (int) Math.ceil((double)tmpList.size()/this.numOfThread);
			int numOfRecord = 0;
			
			// Loop through the Movie Data list
			Iterator it = tmpList.entrySet().iterator();
			Map<Integer, movieData> tokenList = new<Integer, movieData>HashMap();
			
			while (it.hasNext())
			{
				numOfRecord++;
				Map.Entry<Integer, movieData> pair = (Map.Entry<Integer, movieData>) it.next();
				tokenList.put(pair.getKey(), pair.getValue());
				
				if (numOfRecord > maxNumOfRecord)
				{
					this.movieDataList.add(tokenList);
					tokenList = new<Integer, movieData>HashMap();
					numOfRecord = 0;
				}
				else if(it.hasNext() == false)
				{
					this.movieDataList.add(tokenList);
				}
			}
			
			System.out.println("Done");
			/*
			System.out.println("Total number of record:" + tmpList.size());
			System.out.println("Each Data list has " + Integer.toString(maxNumOfRecord) +" Records");
			System.out.println("Number of thread:" + this.numOfThread);
			*/
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void loadGenresData()
	{
		try 
		{
			System.out.print("Load title_basics_tsv_gz/data.tsv file...");
			// Read Movie Data
			File file = new File("title_basics_tsv_gz/data.tsv");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
			String st;
			
			int lineCount = 0;
			
			// Skip the first line
			st = br.readLine();
			
			List<String> tmpGenresList = new ArrayList<String>();
			List<movieGenresData> tmpMovieGenresList = new ArrayList<movieGenresData>();
			
			// Read the content
			while ((st = br.readLine()) != null)
			{
			    String[] str_array = st.split("\t");
			    
			    String genres = str_array[8];
			    String[] genres_token_list = genres.split(",");
			    int movieId = Integer.parseInt(str_array[0].substring(2));
			    
			    for (String token: genres_token_list )
			    {
			    	lineCount++;
			    	if (!tmpGenresList.contains(token))
			    	{
			    		tmpGenresList.add(token);
			    	}
			    	
			    	tmpMovieGenresList.add(new movieGenresData(movieId, (tmpGenresList.indexOf(token)+1)));
			    }
			}
			
			this.movieGenresDataList = new ArrayList<ArrayList<movieGenresData>>();
			
			int maxNumOfRecord = (int) Math.ceil((double)tmpMovieGenresList.size()/this.numOfThread);
			for (int i = 0; i < tmpMovieGenresList.size(); i++)
			{
				if( i % maxNumOfRecord ==0)
				{
					movieGenresDataList.add(new ArrayList<movieGenresData>());
				}
				
				movieGenresDataList.get((i / maxNumOfRecord)).add(tmpMovieGenresList.get(i));
			}
			
			this.genresList = tmpGenresList;
			
			/*
			System.out.println("Size of Genres List:" + tmpGenresList.size());
			System.out.println("move genres data Number of record:" + movieGenresDataList.size());
			System.out.println("genres Number of record:" + genresList.size());
			System.out.println("Size of Movie Genres List:" + tmpMovieGenresList.size());*/
			
			System.out.println("Done");
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void updateGenresData()
	{
		int oneThread = 1;
		
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < oneThread; i++)
		{
			this.dataEngineList.get(i).setGenresList(this.genresList);
			this.dataEngineList.get(i).setUpdateDataType(sqlTableType.UPDATE_GENRES);
			threadList.add(new Thread(this.dataEngineList.get(i)));
		}
		
		this.genresList.clear();
		
		for(int i =0; i < oneThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < oneThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void updateMovieGeneresData()
	{
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.dataEngineList.size(); i++)
		{
			this.dataEngineList.get(i).setMovieGenresDataList(this.movieGenresDataList.get(i));
			this.dataEngineList.get(i).setUpdateDataType(sqlTableType.UPDATE_MOVIE_GENRES);
			threadList.add(new Thread(this.dataEngineList.get(i)));
		}
		
		this.movieGenresDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void loadNameData()
	{
		// Read Name Data		
		// Read the content
		try 
		{
			System.out.print("Load name_basics_tsv_gz/data.tsv file... ");
			File file = new File("name_basics_tsv_gz/data.tsv");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
			String st;
			
			List<nameData> tmpRecordList = new ArrayList<nameData>();
			int lineCount = 0;
			st = br.readLine();
			
			while ((st = br.readLine()) != null)
			{
				lineCount++;
				String[] str_array = st.split("\t");
				
				nameData tmpRecord = new nameData(
						Integer.parseInt(str_array[0].substring(2)), 
						str_array[1], 
						str_array[2],
						str_array[3]
				);
								
				tmpRecordList.add(tmpRecord);
			}
						
			int maxNumOfRecord = (int) Math.ceil((double)tmpRecordList.size()/this.numOfThread);
			
			for (int i = 0; i < tmpRecordList.size(); i++)
			{
				if( i % maxNumOfRecord ==0)
				{
					this.nameDataList.add(new ArrayList<nameData>());
				}
				
				this.nameDataList.get((i / maxNumOfRecord)).add(tmpRecordList.get(i));
			}
			
			/*
			int sumOfRecord = 0;
			
			for (int i = 0; i < nameDataList.size(); i++)
			{
				sumOfRecord += nameDataList.get(i).size();
			}
			
			System.out.println("Total number of record:" + tmpRecordList.size());
			System.out.println("Each Data list has " + Integer.toString(maxNumOfRecord) +" Records");
			System.out.println("Number of thread:" + this.numOfThread);
			System.out.println("Stored list number:" + this.nameDataList.size());
			System.out.println("Sperated List records:" + sumOfRecord);
			*/
			System.out.println("Done");
		} 
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public ArrayList<ArrayList<personData>> dataPartition(List<personData> inputList)
	{
		ArrayList<ArrayList<personData>> rtnList = new ArrayList<ArrayList<personData>>();
		
		int maxNumOfRecord = (int) Math.ceil((double)inputList.size()/this.numOfThread);
		for (int i = 0; i < inputList.size(); i++)
		{
			if( i % maxNumOfRecord ==0)
			{
				rtnList.add(new ArrayList<personData>());
			}
			
			rtnList.get((i / maxNumOfRecord)).add(inputList.get(i));
		}
		
		return rtnList;
	}
	
	public void loadPersonData()
	{
		// Read Name Data		
		// Read the content
		try 
		{
			System.out.print("Load title_principals_tsv_gz/data.tsv file... ");
			File file = new File("title_principals_tsv_gz/data.tsv");
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF8"));
			String st;
			
			int lineCount = 0;
			int movieId;
			int nameId;
			
			List<personData> tmpActorList = new ArrayList<personData>();
			List<personData> tmpDirectorList = new ArrayList<personData>();
			List<personData> tmpProducerList = new ArrayList<personData>();
			List<personData> tmpWriterList = new ArrayList<personData>();
			
			st = br.readLine();
			
			while ((st = br.readLine()) != null)
			{
				lineCount++;
				
				String[] str_array = st.split("\t");

				movieId = Integer.parseInt(str_array[0].substring(2));
				nameId = Integer.parseInt(str_array[2].substring(2));
				
				if (str_array[3].equals("director"))
				{
					tmpDirectorList.add(new personData(movieId, nameId));
				}
				else if(str_array[3].equals("producer"))
				{
					tmpProducerList.add(new personData(movieId, nameId));
				}
				else if(str_array[3].equals("actor") || str_array[3].equals("actress"))
				{
					tmpActorList.add(new personData(movieId, nameId));
				}
				else if(str_array[3].equals("writer"))
				{
					tmpWriterList.add(new personData(movieId, nameId));
				}
				
			}

			this.directorDataList = dataPartition(tmpDirectorList);
			this.producerDataList = dataPartition(tmpProducerList);
			this.actorDataList = dataPartition(tmpActorList);
			this.writerDataList = dataPartition(tmpWriterList);
			
			/*
			int directorSize = 0;
			int producerSize = 0;
			int actorSize = 0;
			int writerSize = 0;
			for (int i=0; i < this.numOfThread; i++)
			{
				directorSize += this.directorDataList.get(i).size();
				producerSize += this.producerDataList.get(i).size();
				actorSize += this.actorDataList.get(i).size();
				writerSize += this.writerDataList.get(i).size();
			}
			
			System.out.println("Total number of record:" + lineCount);
			int sum = tmpDirectorList.size() + tmpProducerList.size() + tmpActorList.size() + tmpWriterList.size();
			System.out.println("Number of total name in the list:" + sum);
			System.out.println("Orignal director list:" + tmpDirectorList.size() + " Partition size:" + directorSize);
			System.out.println("Orignal producer list:" + tmpProducerList.size() + " Partition size:" + producerSize);
			System.out.println("Orignal actor list:" + tmpActorList.size() + " Partition size:" + actorSize);
			System.out.println("Orignal writer list:" + tmpWriterList.size() + " Partition size:" + writerSize);
			*/
			System.out.println("Done");
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void updateMovieData()
	{
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.dataEngineList.size(); i++)
		{
			this.dataEngineList.get(i).setMovieDataList(this.movieDataList.get(i));
			this.dataEngineList.get(i).setUpdateDataType(sqlTableType.UPDATE_MOVIE);
			threadList.add(new Thread(this.dataEngineList.get(i)));
		}
		
		this.movieDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void updateNameData()
	{
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.dataEngineList.size(); i++)
		{
			this.dataEngineList.get(i).setNameDataList(this.nameDataList.get(i));
			this.dataEngineList.get(i).setUpdateDataType(sqlTableType.UPDATE_NAME);
			threadList.add(new Thread(this.dataEngineList.get(i)));
		}
		
		this.nameDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void updateDirectorData()
	{
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.dataEngineList.size(); i++)
		{
			this.dataEngineList.get(i).setDirectorDataList(this.directorDataList.get(i));
			this.dataEngineList.get(i).setUpdateDataType(sqlTableType.UPDATE_DIRECTOR);
			threadList.add(new Thread(this.dataEngineList.get(i)));
		}
		
		this.directorDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void updateProducerData()
	{
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.dataEngineList.size(); i++)
		{
			this.dataEngineList.get(i).setProducerDataList(this.producerDataList.get(i));
			this.dataEngineList.get(i).setUpdateDataType(sqlTableType.UPDATE_PRODUCER);
			threadList.add(new Thread(this.dataEngineList.get(i)));
		}
		
		this.producerDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}	
	
	public void updateActorData()
	{
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.dataEngineList.size(); i++)
		{
			this.dataEngineList.get(i).setActorDataList(this.actorDataList.get(i));
			this.dataEngineList.get(i).setUpdateDataType(sqlTableType.UPDATE_ACTOR);
			threadList.add(new Thread(this.dataEngineList.get(i)));
		}
		
		this.actorDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void updateWriterData()
	{
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.dataEngineList.size(); i++)
		{
			this.dataEngineList.get(i).setWriterDataList(this.writerDataList.get(i));
			this.dataEngineList.get(i).setUpdateDataType(sqlTableType.UPDATE_WRITER);
			threadList.add(new Thread(this.dataEngineList.get(i)));
		}
		
		this.writerDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}	
	
	public void updatePersonData()
	{
		List<Thread> threadList= new ArrayList<Thread>();
		
		for(int i =0; i < this.dataEngineList.size(); i++)
		{
			this.dataEngineList.get(i).setDirectorDataList(this.directorDataList.get(i));
			this.dataEngineList.get(i).setProducerDataList(this.producerDataList.get(i));
			this.dataEngineList.get(i).setActorDataList(this.actorDataList.get(i));
			this.dataEngineList.get(i).setWriterDataList(this.writerDataList.get(i));
			this.dataEngineList.get(i).setUpdateDataType(sqlTableType.UPDATE_PERSON);
			threadList.add(new Thread(this.dataEngineList.get(i)));
		}
		
		this.directorDataList.clear();
		this.producerDataList.clear();
		this.actorDataList.clear();
		this.writerDataList.clear();
		
		for(int i =0; i < numOfThread; i++)
		{
			threadList.get(i).start();
		}
		
		for(int i =0; i < numOfThread; i++)
		{
			try 
			{
				threadList.get(i).join();
			} 
			catch (InterruptedException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void updateDataToDataBase()
	{
		System.out.println("Updating database... ");
    	long start;
    	long finish;
    	long timeElapsed;
		
    	this.loadGenresData();
    	
    	start = System.currentTimeMillis();
		this.updateGenresData();
    	finish = System.currentTimeMillis();
    	timeElapsed = finish - start;
    	System.out.println("Genres list Done(" + 
    			Double.toString((double)timeElapsed/1000.0) +
    			" sec)");
    	
    	start = System.currentTimeMillis();
		this.updateMovieGeneresData();;
    	finish = System.currentTimeMillis();
    	timeElapsed = finish - start;
    	System.out.println("Movie Genres Data list Done(" + 
    			Double.toString((double)timeElapsed/1000.0) +
    			" sec)");
		 	
    	start = System.currentTimeMillis();
    	this.loadMovieData();
    	this.updateMovieData();
    	finish = System.currentTimeMillis();
    	timeElapsed = finish - start;
    	System.out.println("Movie Done(" + 
    			Double.toString((double)timeElapsed/1000.0) +
    			" sec)");
    	
    	start = System.currentTimeMillis();
    	this.loadNameData();
    	this.updateNameData();
    	finish = System.currentTimeMillis();
    	timeElapsed = finish - start;
    	System.out.println("Name Done(" + 
    			Double.toString((double)timeElapsed/1000.0) +
    			" sec)");
    	
    	this.loadPersonData();
    	
    	start = System.currentTimeMillis();
    	this.updateDirectorData();
    	finish = System.currentTimeMillis();
    	timeElapsed = finish - start;
    	System.out.println("Director Done(" + 
    			Double.toString((double)timeElapsed/1000.0) +
    			" sec)");
    	
    	start = System.currentTimeMillis();
    	this.updateProducerData();
    	finish = System.currentTimeMillis();
    	timeElapsed = finish - start;
    	System.out.println("Producer Done(" + 
    			Double.toString((double)timeElapsed/1000.0) +
    			" sec)");
    	
    	start = System.currentTimeMillis();
    	this.updateWriterData();
    	finish = System.currentTimeMillis();
    	timeElapsed = finish - start;
    	System.out.println("Writer Done(" + 
    			Double.toString((double)timeElapsed/1000.0) +
    			" sec)");
    	
    	start = System.currentTimeMillis();
    	this.updateActorData();
    	finish = System.currentTimeMillis();
    	timeElapsed = finish - start;
    	System.out.println("Actor Done(" + 
    			Double.toString((double)timeElapsed/1000.0) +
    			" sec)");
	}
	
	public void readDataFile(String fileName)
	{
		try 
		{
			// Read Movie Data
			File file = new File(fileName);
			BufferedReader br = new BufferedReader(new FileReader(file));
			String st;
			long count = 0;
			while ((st = br.readLine()) != null)
			{
				count ++;
			   
			    String[] str_array = st.split("\t");
			    for(String token: str_array)
			    	System.out.print(token + " ");
			    
			    if (count == 4)
			    {
			    	break;
			    }
			    
			    System.out.println();
			}
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		
		// Number of thread is 100
		SqlEngine engine = new SqlEngine(25);
		engine.connectDatabase();
		
		// Clear Table 
		engine.executeSQL("use hw1");
		
		engine.executeSQL("delete from actorList");
		System.out.println("Clear actorList database... Done");
		engine.executeSQL("delete from directorList");
		System.out.println("Clear directorList database... Done");
		engine.executeSQL("delete from writerList");
		System.out.println("Clear writerList database... Done");
		engine.executeSQL("delete from producerList");
		System.out.println("Clear producerList database... Done");
		engine.executeSQL("delete from movieGenresList");
		System.out.println("Clear movieGenresList database... Done");
		engine.executeSQL("delete from genresList");
		System.out.println("Clear genresList database... Done");
		engine.executeSQL("delete from movieList");
		System.out.println("Clear movieList database... Done");
		engine.executeSQL("delete from nameList");
		System.out.println("Clear nameList database... Done");
		
		// Read Data from IMDB database and load to SQL database
		engine.updateDataToDataBase();
		
		// insert invalid record
		System.out.println();
		System.out.println("*************************************** Forcing Error! ***************************************");
		
		Map<String, Object> sqlMap = new HashMap<String, Object>();
		sqlMap.put("ID", 40);
		sqlMap.put("genres", "11111111111111111111111111111111111111111111111111111111111111111111111111111111111");
		engine.insertToTable("genresList", sqlMap);
		System.out.println();
		
		sqlMap = new HashMap<String, Object>();
		sqlMap.put("ID", null);
		sqlMap.put("genres", "new_type");
		engine.insertToTable("genresList", sqlMap);
		System.out.println();
		
		System.out.println("Homework#1 Completed");
		
		// closing data base
		engine.closeDatabase();
	}

}
